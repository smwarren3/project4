% EOC 6189 Project 4
% S. Matthew Warren
% Due 22 April 2019


%% Setup
%Create a 2pi x 2pi grid
Lx = 2*pi; Ly = Lx;

% Use 31 Points
Nx = 31; Ny = Nx;N=Nx;

% h=dx=dy
h = Lx/(Nx-1);

%create x and y vectors
x = linspace(0,Lx,Nx); y = linspace(0,Ly,Ny);
%Create wrapping ghost cells for periodic boundary condition
x=[x(length(x)-1),x,x(2)];
y=[y(length(y)-1),y,y(2)];

%provided time step, max time, and viscosity values
dt = 0.01;tmax=10;t=0:dt:tmax;nu=0.01;rho=1;

%Initialize variables used in loops
phi=zeros([length(x) length(y) length(t)]);
u=phi;v=phi;P=ones(size(phi));
ustar=zeros([length(x) length(y)]);vstar=ustar;
uanalytical=phi;vanalytical=phi;

%Initial U conditions
u(:,:,1) = cos(x)'.*sin(y); v(:,:,1) = -sin(x)'.*cos(y);

% Initial Pressure
P(:,:,1) = (cos(2.*x)+cos(2.*y'))./4;
%Pstar=zeros([length(x) length(y)]);


for k=1:length(t)
uanalytical(:,:,k) = (cos(x)'.*sin(y)).*exp(-2*nu*t(k));
vanalytical(:,:,k) = -(sin(x)'.*cos(y)).*exp(-2*nu*t(k));
end

%% Solve for U*

for n=1:length(t)-1 % iterate through time steps
     for j=2:Ny+1 %i=j=1,2,end,end-1 are ghost cells
         for i=2:Nx+1
                
 %diffusion
 A = (nu/(h^2)).*[(u(i+1,j,n)+u(i-1,j,n)-(4*u(i,j,n))+u(i,j+1,n)+u(i,j-1,n));...
     (v(i+1,j,n)+v(i-1,j,n)-(4*v(i,j,n))+v(i,j+1,n)+v(i,j-1,n))];
 
 %advection
 B = [(u(i,j,n)*(u(i+1,j,n)-u(i-1,j,n)))+(v(i,j,n)*(u(i,j+1,n)-u(i,j-1,n)));...
     (u(i,j,n)*(v(i+1,j,n)-v(i-1,j,n)))+(v(i,j,n)*(v(i,j+1,n)-v(i,j-1,n)))]./(2*h);

%combine to solve for u* and v*, (or U*) 
Ustar = ((A+B).*dt)+[u(i,j,n);v(i,j,n)];
ustar(i,j)=Ustar(1);
vstar(i,j)=Ustar(2);
        end
    end

    
ustar(:,1)=ustar(:,end-2);ustar(:,end)=ustar(:,3);
ustar(1,:)=ustar(end-2,:);ustar(end,:)=ustar(3,:);
vstar(:,1)=vstar(:,end-2);vstar(:,end)=vstar(:,3);
vstar(1,:)=vstar(end-2,:);vstar(end,:)=vstar(3,:);



figure(1);
subplot(1,3,1);
quiver(uanalytical(2:N+1,2:N+1,n+1),vanalytical(2:N+1,2:N+1,n+1));
title('analytical')
subplot(1,3,2);
quiver(u(2:N+1,2:N+1,n),v(2:N+1,2:N+1,n));
title('last')
subplot(1,3,3);
quiver(ustar(2:N+1,2:N+1),vstar(2:N+1,2:N+1));
title('star')


 
    err = 1;dTau=10^-4;count=1; %Initialize err and Tau
       
   while err > 10^-6 % Check to see if error is within tolerance       
        
       for j=2:Ny+1 %i=j=1,2,end,end-1 are ghost cells
       for i=2:Nx+1     
P(i,j,n+1) = ((((-1/h^2)*(P(i-1,j,n)+P(i+1,j,n)-(4*P(i,j,n))+P(i,j-1,n)+P(i,j+1,n)))...
    + (rho/(2*h*dt))*(ustar(i+1,j)-ustar(i-1,j)+vstar(i,j+1)-vstar(i,j-1)))*dTau)+P(i,j,n);
       end
       end
P(:,1,n+1)=P(:,end-1,n+1);P(:,end,n+1)=P(:,2,n+1);
P(1,:,n+1)=P(end-1,:,n+1);P(end,:,n+1)=P(2,:,n+1);    
        % Find error
err=sqrt(sum(sum((P(2:Nx+1,2:Ny+1,n+1)-P(2:Nx+1,2:Ny+1,n)).^2)));count=count+1;
if err>10^-6
    P(:,:,n)=P(:,:,n+1);
end
    end


% Solve for U(:,:,n+1)

        for j=2:Ny+1 %i=j=1,2,end,end-1 are ghost cells
        for i=2:Nx+1 
            
UnPlusOne = ((-dt/(rho*2*h)).*[P(i+1,j,n+1)-P(i-1,j,n+1);P(i,j+1,n+1)-P(i,j-1,n+1)])...
    +[ustar(i,j);vstar(i,j)];

u(i,j,n+1)=UnPlusOne(1);
v(i,j,n+1)=UnPlusOne(2);

u(:,1,n+1)=u(:,end-2,n+1);u(:,end,n+1)=u(:,3,n+1);
u(1,:,n+1)=u(end-2,:,n+1);u(end,:,n+1)=u(3,:,n+1);
v(:,1,n+1)=v(:,end-2,n+1);v(:,end,n+1)=v(:,3,n+1);
v(1,:,n+1)=v(end-2,:,n+1);v(end,:,n+1)=v(3,:,n+1);

        end
        end
end
        
%% Cut out ghost cells
truncate = [1 length(x)];
u(truncate,:,:)=[];u(:,truncate,:)=[];
uanalytical(truncate,:,:)=[];uanalytical(:,truncate,:)=[];
v(truncate,:,:)=[];v(:,truncate,:)=[];
vanalytical(truncate,:,:)=[];vanalytical(:,truncate,:)=[];
phi(truncate,:,:)=[];phi(:,truncate,:)=[];
P(truncate,:,:)=[];P(:,truncate,:)=[]; 
x(truncate)=[];
y(truncate)=[];

%% See where we are at
figure(1)
vidObj = VideoWriter('P4Test3.mp4', 'MPEG-4');
open(vidObj);

a = tic;
for k = 1:length(t)
    phi(:,:,k)=(u(:,:,k).^2+v(:,:,k).^2).^0.5;
    contourf(x,y,P(:,:,k));colormap(jet);colorbar;hold on
    quiver(x,y,u(:,:,k),v(:,:,k),sin(2*pi*t(k)/tmax),'k');
    quiver(x,y,uanalytical(:,:,k),vanalytical(:,:,k),sin(2*pi*t(k)/tmax),'c'); hold off
    axis 'tight'
    xlabel('x');ylabel('y');title('Quiver test')
        drawnow
         M(k) = getframe(gcf);
         writeVideo(vidObj,M(k));
end
close(vidObj);

