% EOC 6189 Project 4
% S. Matthew Warren
% Due 22 April 2019

% This is a seperate version in an attempt to isolate bugs. 
% The initial version calculated U as [u v].
% This code leaves u and v as sererate variables throughout.

%% Setup
%Create a 2pi x 2pi grid
Lx = 2*pi; Ly = Lx;

% Use 31 Points across x and y directions
Nx = 31;Ny = Nx;N=Nx;

% h=dx=dy
h = Lx/(Nx-1);

%create x and y vectors
x = linspace(0,Lx,Nx); y = linspace(0,Ly,Ny);
%Create wrapping ghost cells for periodic boundary condition
x=[x(end-1),x,x(2)];
y=[y(end-1),y,y(2)];

%provided time step, max time, and viscosity values
dt = 0.01;tmax=10;t=0:dt:tmax;nu=0.01;rho=1;

%Initialize Velocity Variables
u=zeros([length(x) length(y) length(t)]);v=u; %U
ustar=zeros([length(x) length(y)]);vstar=ustar;
ustarstar=ustar;vstarstar=ustar;
mag = zeros([Nx Ny length(t)]);
uanalytical=u;vanalytical=v; %Analytical solution
div = zeros(size(u));divsum=zeros(length(t),1);divavg=divsum;
%Initialize Pressure 
P=rand(size(u)); %Pressure 
P(:,:,1)=-(rho/4).*(cos(2.*x)' + cos(2.*y)); %Should not be necessary
%Initial U conditions
u(:,:,1) = cos(x)'.*sin(y); v(:,:,1) = -sin(x)'.*cos(y);

%Calculate analytical solution
for k=1:length(t)
uanalytical(:,:,k) = (cos(x)'.*sin(y)).*exp(-2*nu*t(k));
vanalytical(:,:,k) = -(sin(x)'.*cos(y)).*exp(-2*nu*t(k));
end

%% Solve for U*

for n=1:length(t)-1
%% Solve for U* -- Diffusion
u(:,1,n)=u(:,end-2,n);u(:,end,n)=u(:,3,n);
u(1,:,n)=u(end-2,:,n);u(end,:,n)=u(3,:,n);
v(:,1,n)=v(:,end-2,n);v(:,end,n)=v(:,3,n);
v(1,:,n)=v(end-2,:,n);v(end,:,n)=v(3,:,n);

 %Split 1 (U*)     
     for j=2:Ny+1 %i=j=1,2,end,end-1 are ghost cells
     for i=2:Nx+1
dudx=(u(i+1,j,n)-u(i-1,j,n))/(2*h); dvdy=(v(i,j+1,n)-v(i,j-1,n))/(2*h);
div(i,j,n)=dudx+dvdy;
 du2dx2 = (u(i+1,j,n)-2*u(i,j,n)+u(i-1,j,n))/h^2;
 du2dy2 = (u(i,j+1,n)-2*u(i,j,n)+u(i,j-1,n))/h^2;
 dv2dx2 = (v(i+1,j,n)-2*v(i,j,n)+v(i-1,j,n))/h^2;
 dv2dy2 = (v(i,j+1,n)-2*v(i,j,n)+v(i,j-1,n))/h^2;
 ustar(i,j) = (nu*(du2dx2+du2dy2)*dt)+u(i,j,n);
 vstar(i,j) = (nu*(dv2dx2+dv2dy2)*dt)+v(i,j,n);

     end
     end
divsum(n)=sum(sum(abs(div(:,:,n))));divavg(n)=divsum(n)/N^2;

%Remake ghost cells for ustar
ustar(:,1)=ustar(:,end-2);ustar(:,end)=ustar(:,3);
ustar(1,:)=ustar(end-2,:);ustar(end,:)=ustar(3,:);
vstar(:,1)=vstar(:,end-2);vstar(:,end)=vstar(:,3);
vstar(1,:)=vstar(end-2,:);vstar(end,:)=vstar(3,:);
 
 
%% Split 2 (U**) -- Advection

     for j=2:Ny+1 %i=j=1,2,end,end-1 are ghost cells
     for i=2:Nx+1                
 dudx=(ustar(i+1,j)-ustar(i-1,j))/(2*h);
 dudy=(ustar(i,j+1)-ustar(i,j-1))/(2*h);
 dvdx=(vstar(i+1,j)-vstar(i-1,j))/(2*h);
 dvdy=(vstar(i,j+1)-vstar(i,j-1))/(2*h);
 
 ustarstar(i,j) = (-((ustar(i,j)*dudx)+(vstar(i,j)*dvdy))*dt)+ustar(i,j);
 vstarstar(i,j) = (-((ustar(i,j)*dudx)+(vstar(i,j)*dvdy))*dt)+vstar(i,j);
     end
     end
%Remake ghost cells for ustar
ustarstar(1,:)=ustarstar(end-2,:);ustarstar(end,:)=ustarstar(3,:);
vstarstar(:,1)=vstarstar(:,end-2);vstarstar(:,end)=vstarstar(:,3);
vstarstar(1,:)=vstarstar(end-2,:);vstarstar(end,:)=vstarstar(3,:);
ustarstar(:,1)=ustarstar(:,end-2);ustarstar(:,end)=ustarstar(:,3);
%}

%for without Pressure Projection
 u(:,:,n+1)=ustarstar;
 v(:,:,n+1)=vstarstar;


%% Check status with quiver plot

figure(1);
subplot(2,2,1);
quiver(uanalytical(2:N+1,2:N+1,n),vanalytical(2:N+1,2:N+1,n));
title(['Analytical solution t=' num2str(t(n))]);axis([1 Nx+2 1 Ny+2]);
subplot(2,2,2);
quiver(u(2:N+1,2:N+1,n),v(2:N+1,2:N+1,n));
title('last');axis([1 Nx+2 1 Ny+2]);
subplot(2,2,3);
quiver(ustarstar(2:N+1,2:N+1),vstarstar(2:N+1,2:N+1));
title('starstar');axis([1 Nx+2 1 Ny+2]);
subplot(2,2,4);
quiver(ustar(2:N+1,2:N+1),vstar(2:N+1,2:N+1));
title('star');axis([1 Nx+2 1 Ny+2]);
%}

%% Solve for P(n+1)
%{
%Initialize error, dTau, and create a count to see how many iterations have
%been spend on the time-step when debugging
err = 1;dTau=10^-8;count=1;diff=zeros([31 31]);lasterr=0;
     
while err > 10^-10 % Tolerance set here   
      
       for j=2:Ny+1 %i=j=1 & end are ghost cells
       for i=2:Nx+1     
           
%Calculate derivatives used in this loop
dP2dx2=(P(i+1,j,n)-(2*P(i,j,n))+P(i-1,j,n))/(h^2); %dP^2/dx^2
dP2dy2=(P(i,j+1,n)-(2*P(i,j,n))+P(i,j-1,n))/(h^2); %dP^2/dy^2

duStarStardx=(ustarstar(i+1,j)-ustarstar(i-1,j))/(2*h); %du**/dx
dvStarStardy=(vstarstar(i,j+1)-vstarstar(i,j-1))/(2*h); %dv**/dy


%used to calculate P(n+1)
fijn = (rho/dt)*(duStarStardx + dvStarStardy);

%calculate P(i,j,n+1) 
P(i,j,n+1) = ((-(dP2dx2+dP2dy2)+fijn)*dTau)+P(i,j,n);

diff(i-1,j-1)=P(i,j,n+1)-P(i,j,n);
       end
       end
       
%Make ghost cells for P(n+1)
P(:,1,n+1)=P(:,end-2,n+1);P(:,end,n+1)=P(:,3,n+1);
P(1,:,n+1)=P(end-2,:,n+1);P(end,:,n+1)=P(3,:,n+1);    

%Pcounts(:,:,count)=P(:,:,n+1);

%Calculate error and count to see number of iterations in debugging
err=sqrt(sum(sum(diff.^2))/(N^2));
count=count+1;
errdif=err-lasterr;


%Check the error is decreasing
if err>lasterr && mod(count,1000)==0;'P(n+1) is not converging' 
else if errdif == 0 && mod(count,1000)==0;['Error is static ', num2str(count)]
    else if mod(count,1000)==0 'P(n+1) is converging'
    end
    end
end
lasterr = err;

%if P has not reached 'stead-state' rename P(n+1) P(n) and repeat the loop
if err>(10^-6)
    P(:,:,n)=P(:,:,n+1);
end
end
n
count
%}

%% Find U(n+1)

%FOR TESTING U(n+1) given P(n+1)
P(:,:,n+1) = -(rho/4).*(cos(2.*x)' + cos(2.*y)).*((exp(-2*nu*t(n+1)))^2);



        for j=2:Ny+1 %i=j=1,2,end,end-1 are ghost cells
        for i=2:Nx+1 

%find dP(n+1)/dx
dPN1dx = (P(i+1,j,n+1)-P(i-1,j,n+1))/(2*h);
%find dP(n+1)/dy
dPN1dy = (P(i,j+1,n+1)-P(i,j-1,n+1))/(2*h);

% Calculate U(n+1)

u(i,j,n+1)= (-dt*dPN1dx/rho)+ustarstar(i,j);
v(i,j,n+1)= (-dt*dPN1dy/rho)+vstarstar(i,j);

%Remake ghost cells for next time step
u(:,1,n+1)=u(:,end-2,n+1);u(:,end,n+1)=u(:,3,n+1);
u(1,:,n+1)=u(end-2,:,n+1);u(end,:,n+1)=u(3,:,n+1);
v(:,1,n+1)=v(:,end-2,n+1);v(:,end,n+1)=v(:,3,n+1);
v(1,:,n+1)=v(end-2,:,n+1);v(end,:,n+1)=v(3,:,n+1);

        end
        end
%}
mag(:,:,n) = ((u(2:N+1,2:N+1,n).^2+v(2:N+1,2:N+1,n).^2).^0.5);
end


%% Cut out ghost cells

truncate = [1 2 length(x)-1 length(x)];
u(truncate,:,:)=[];u(:,truncate,:)=[];
ustar(truncate,:,:)=[];ustar(:,truncate,:)=[];
ustarstar(truncate,:,:)=[];ustarstar(:,truncate,:)=[];
uanalytical(truncate,:,:)=[];uanalytical(:,truncate,:)=[];
v(truncate,:,:)=[];v(:,truncate,:)=[];
vstar(truncate,:,:)=[];vstar(:,truncate,:)=[];
vstarstar(truncate,:,:)=[];vstarstar(:,truncate,:)=[];
vanalytical(truncate,:,:)=[];vanalytical(:,truncate,:)=[];
P(truncate,:,:)=[];P(:,truncate,:)=[]; 
x(truncate)=[];
y(truncate)=[];
%}
%% See where we are at

figure(5)
vidObj = VideoWriter('DivU_divergence+advection.mp4', 'MPEG-4');
for k=1:10:1000
contourf(div(:,:,k));colorbar;
M(k) = getframe(gcf);
open(vidObj)
writeVideo(vidObj,M(k));
end
close(vidObj)
%}
%%
%{
figure(3)
vidObj = VideoWriter('T401.mp4', 'MPEG-4');
open(vidObj);
a = tic;
for k = 1:130
    contourf(x,y,P(:,:,k));colormap(jet);colorbar;caxis([0 0.4]);hold on
    quiver(x,y,u(:,:,k),v(:,:,k),'k'); hold on
    %quiver(x,y,uanalytical(:,:,k),vanalytical(:,:,k),'c'); hold off
    axis([0 Lx 0 Ly])
    xlabel('x');ylabel('y');
    title(['U quiver, |P| contours t=' num2str(t(k))])
        drawnow
         M(k) = getframe(gcf);
         writeVideo(vidObj,M(k));
end
close(vidObj);
%}
